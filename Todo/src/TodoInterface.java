import java.text.ParseException;

public interface TodoInterface {

    void addTask() throws ParseException;

    void display();

    void deleteTask();

    void markCompleted();

    void groupByDate();

    void search();
}
